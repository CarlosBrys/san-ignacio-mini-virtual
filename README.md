***“Modelo Virtual de la Reducción Jesuítica de San Ignacio Miní”***
(Declarada patrimonio cultural de la humanidad por la UNESCO


Se construyó un modelo que permite la representación del escenario virtual de un tema real, a través de la investigación del  el “Lenguaje de Modelado de Realidad Virtual” (VRML), pero que permite integrar plenamente los conceptos de
Iluminación, Textura, Reconstrucción de Superficies.

Herramientas
Para el desarrollo del escenario virtual se utilizará el Lenguaje de Modelado de Realidad Virtual (VRML) versión 2.
La herramienta de generación del escenario virtual fue el programa Internet Space Builder version 3.0
La herramienta de autoría para el desarrollo de contenido VRML fue VrmlPad versión 2.1
El software cliente para el explorador de Internet que permite visualizar y explorar los escenarios virtuales utilizado es Cortona VRML Web3D viewer.
Todo el software mencionado es de la empresa ParallelGraphics.